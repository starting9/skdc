<x-backend.layouts.master>
    <div class="container">
        <div class="card-header">Add New Digital Currency
            <a  href="{{ route('coins.index') }}" class="btn btn-sm btn-primary float-end">Coin List</a>
        </div>

        <div class="card-body">
            <form action="{{ route('coins.store') }}" method="post">
                @csrf

                <div class="mb-3">
                  <label for="name" class="form-label">Name</label>
                  <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}" />

                  @error('name')
                    <span class="text-danger">{{ $message }}</span>
                  @enderror
                </div>

                <div class="mb-3">
                    <label for="value" class="form-label">Value</label>
                    <input type="float" class="text form-control"  id="value" name="value" />

                    @error('value')
                      <span class="text-danger"> {{ $message }}</span>
                    @enderror
                  </div>

                <button type="submit" class="btn btn-primary">Save</button>
                
                <a class="btn btn-danger" href="{{ route('coins.index') }}" ><i class="fa-solid fa-x"> </i>  Cancel</a>

              </form>
        </div>
    </div>
</x-backend.layouts.master>
