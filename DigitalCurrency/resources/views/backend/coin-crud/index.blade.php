<x-backend.layouts.master>
    <div class="container">
       <div class="card-header">
           Coin List

        <a class="btn btn-sm btn-success float-end"  href="{{ route('coins.create') }}">Add Product</a>
        

        @if (session('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong> {{ session('message') }} </strong>
            </div>
        @endif
       <div class="card-body">
       <table id="datatablesSimple" style="width: 100%">
            <thead>
                <tr>
                    <th class="text-center">SL</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Value</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            
            <tbody>
                @foreach ($coins as $coin)
                    <tr>
                        <td class="text-center">{{ $loop->iteration }}</td>
                        <td class="text-center">{{ $coin->name }}</td>
                        <td class="text-center">{{ $coin->value }}</td>
                        <td class="text-center">
                            <a href="{{ route('coins.show', $coin->id) }}" class="btn btn-sm btn-primary">Show</a>

                            <a  href="{{ route('coins.edit',  $coin->id) }}" class="btn btn-sm btn-success">Edit</a>

                            <form action="{{ route('coins.destroy', ['coin' => $coin->id]) }}" method="POST" style="display:inline">
                                @csrf
                                @method('delete') 
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete?')">Delete</button>
                             </form>
             
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
       </div>
    </div>
</x-backend.layouts.master>
